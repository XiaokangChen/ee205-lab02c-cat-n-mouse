///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  Xiaokang Chen <xiaokang@hawaii.edu>
/// @date    24 Jan 2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>

#define DEFAULT_MAX_NUMBER 2048

int main( int argc, char* argv[] ) {

   int theMaxValue = DEFAULT_MAX_NUMBER;
   
   //printf("%d\n", argc);

   if (argc > 1) {
      if (atoi(argv[1]) >= 1) {

         theMaxValue = atoi(argv[1]);
  
      } else {

         printf("Please enter a valid number\n");
         return 1;

      }
   }

   int randomNumber = (rand() % theMaxValue) + 1;

   //printf("%d\n", randomNumber); //This prints out the number that the mouse is thinking

   int catGuess;

   do {

      printf("OK cat, I'm thinking of a number from 1 to %d. Make a guess: ", theMaxValue);
      scanf("%d", &catGuess);

      if (catGuess < 1 ) {
      
         printf("You must enter a number that's >= 1\n");
         continue;

      }

      if (catGuess > theMaxValue) {

         printf("You must enter a number that's <= %d\n", theMaxValue);
         continue;

      }

      if (catGuess > randomNumber) {
         
         printf("No cat... the number I'm thinking of is smaller than %d\n", catGuess);
         continue;

      } else if (catGuess < randomNumber) {

         printf("No cat... the number I'm thinking of is larger than %d\n", catGuess);
         continue;

      } else {

         printf("You got me.\n");
         printf(" _._     _,-'""`-._\n");
         printf("(,-.`._,'(       |\\`-/|\n");
         printf("    `-.-' \\ )-`( , o o)\n");
         printf("          `-    \\`_`\\\"'-\n");
         return 0;

      }
   
   } while (catGuess != randomNumber);

}
